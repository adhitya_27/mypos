<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Detail;
use App\Product;
use App\Sale;
use Auth;

class DetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->level == 1) {
            return view('transactions.success');
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new Sale();
        $id = $request->id;
        $total = $request->total;
        $uang = $request->uang;
        $i_total = DB::table('sales')
                  ->where('id', $id)
                  ->update(['total' => $total]);
        $kembalian = $uang - $total;
        foreach (session('cart') as $id => $details) {
            $product = Product::where('id', $id)->get();
            foreach ($product as $products) {
                $old_stock = $products['stock'];
            }
            $s_stock = $details['qty'];
            $k_stock = DB::table('products')->where('id', $id)->update(['stock' => $old_stock-$s_stock]);

            $request->session()->forget('cart');
        }
        return view('transactions.success', [
            'kembalian' => $kembalian
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
