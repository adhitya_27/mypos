<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Product;

class SesiController extends Controller
{
	public function index(Request $request){
    $search = $request->search;
    if ($search == '') {
        $products = Product::all(); 
    } else {
        $products = Product::where('id', $request->search)
        ->orWhere('name', 'like', '%' . $request->search . '%')->get();
    }

    
	return view('transactions.product', [
		'products' => $products,
        'search' => $search
	]);
	}

    // public function search(Request $request){
    // $products = Product::where('id', $request->search)
    // ->orWhere('name', 'like', '%' . $request->search . '%')->get();
    // return view('transactions.product', [
    //     'products' => $products,
    // ]);
    // }

    public function cart()
    {
        return view('transactions.cart');
    }

public function addToCart($id)
    {
        $product = Product::find($id);
 
        if(!$product) {
 
            abort(404);
 
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "name" => $product->name,
                        "qty" => 1,
                        "stock" => $product->stock,
                        "price" => $product->price,
                    ]
            ];
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment qty
        if(isset($cart[$id])) {
 
            $cart[$id]['qty']++;
 
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with qty = 1
        $cart[$id] = [
            "name" => $product->name,
            "qty" => 1,
            "stock" => $product->stock,
            "price" => $product->price,
        ];
 
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }


public function update(Request $request)
    {
        if($request->id and $request->qty)
        {
            $cart = session()->get('cart');
 
            $cart[$request->id]["qty"] = $request->qty;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }
 
    public function remove(Request $request)
    {
        if($request->id) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->id])) {
 
                unset($cart[$request->id]);
 
                session()->put('cart', $cart);
            }
 
            session()->flash('success', 'Product removed successfully');
        }
    }
	// // menampilkan isi session
	// public function tampilkanSession(Request $request) {
	// 	if($request->session()->has('customer')){
	// 	echo "CUSTOMER: ".$request->session()->get('customer')."<br>";
	// 	echo "PRODUCT: ".$request->session()->get('product')."<br>";
	// 	echo "QTY: ".$request->session()->get('qty')."<br>";
	// 	echo "PRICE: ".$request->session()->get('price')."<br>";
	// 	echo "SUBTOTAL: ".$request->session()->get('subtotal')."<br>";
	// 	}else{
	// 		echo 'Tidak ada data dalam session.';
	// 	}
	// }
 
	// // membuat session
	// public function buatSession(Request $request) {

	// 	$id = $request->input('customer');
 //        $cart = session()->get('cart');
 //        // if cart is empty then this the first product
 //        if(!$cart) {
 
 //            $cart = [
 //                    $id => [
 //                        "customer" => $request->input('customer'),
 //                        "product" => $request->input('product'),
 //                        "qty" => $request->input('qty'),
 //                        "price" => $request->input('price'),
 //                        "subtotal" => $request->input('subtotal')
 //                    ] 
 //            ];
 
 //            session()->put('cart', $cart);
 //            echo "Cart Berhasil Disimpan";
 
 //        return view('transactions.cart');
 //        }
 //        // if cart not empty then check if this product exist then increment qty
 //        if(isset($cart[$id])) {
 
 //            $cart[$id]['qty'] = $cart[$id]['qty']+$request->input('qty');
 
 //            session()->put('cart', $cart);
 //        return redirect()->back();
 
 //        }
 
 //        // if item not exist in cart then add to cart with qty = 1
 //        $cart[$id] = [
 //            "customer" => $request->input('customer'),
 //            "product" => $request->input('product'),
 //            "qty" => $request->input('qty'),
 //            "price" => $request->input('price'),
 //            "subtotal" => $request->input('subtotal')
 //        ];
 
 //        session()->put('cart', $cart);
 
 //        return view('transactions.cart');
 

		// session([
		// 	'customer' => $request->input('customer'),
		// 	'product' => $request->input('product'),
		// 	'qty' => $request->input('qty'),
		// 	'price' => $request->input('price'),
		// 	'subtotal' => $request->input('qty') * $request->input('price'),
		// 	'alamat' => 'pbg'
		// ]);
		// $msg =  "Data telah ditambahkan ke session.";
		// $customer = $request->session()->get('customer');
		// $product = $request->session()->get('product');
		// $qty = $request->session()->get('qty');
		// $price = $request->session()->get('price');
		// $subtotal = $request->session()->get('subtotal');

		// echo $request->cookie('qty')."<br>";
		// return view('transactions.cart', [
		// 	'msg' => $msg,
		// 	'customer' => $customer,
		// 	'product' => $product,
		// 	'qty' => $qty,
		// 	'price' => $price,
		// 	'subtotal' => $subtotal,
		// ]);
	
 
	// menghapus session
	public function hapusSession(Request $request) {
		$request->session()->forget('cart');
		echo "Data telah dihapus dari session.";
	}
}
