<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Sale;
use App\Customer;
use App\Detail;
use App\Product;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Sale::all();
        $customer = Customer::all(); 
        $detail = Detail::all();
        $c_detail = count($detail);
        return view('transactions.show', [
            'transactions' => $transactions,
            'customer' => $customer,
            'c_detail' => $c_detail,
            'details' => $detail
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->level == 1) {
            $product = Product::all();
            $customer = Customer::all();
            $transaction = Sale::all();
            return view('transactions.add', [
                'product' => $product,
                'customer' => $customer,
                'transactions' => $transaction
            ]);
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
                    // Step 1 : Create User
                    $transaction = new Sale();
                    $transaction->customer_id = $request->customer;
                    $transaction->total = $request->total;
                    $transaction->save();
        }catch(\Exception $e){
                    DB::rollback();
                    return redirect()->route('home')
                                ->with('warning','Something Went Wrong!');
        }
         
        try{
                    //Step 2 : Stripe Api Call 
         
                    //Step 3 : Amount Charged
                foreach (session('cart') as $id => $details) {
                    $detail = new Detail();
                    $detail->qty = $details['qty'];
                    $detail->price = $details['price'];
                    $detail->subtotal = $details['qty']*$details['price'];
                    $detail->product_id = $id;
                    $detail->sale_id = $transaction->id;
                    $detail->save();
                }
        }catch(\Exception $e){
                    DB::rollback();
                    return redirect()->route('transactions.index')
                                ->with('warning','Something Went Wrong!');
        }
        DB::commit(); // All transaction will commit if statement reach on this
        foreach (session('cart') as $id => $details) {
            $product = Product::where('id', $id)->get();
            foreach ($product as $products) {
                $old_stock = $products['stock'];
            }
            $s_stock = $details['qty'];
            $k_stock = DB::table('products')->where('id', $id)->update(['stock' => $old_stock-$s_stock]);

            $request->session()->forget('cart');
        }
        return redirect()->route('transactions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Sale::where('id', $id)->get();
        $detail = Detail::where('sale_id', $id)->get();
        $totals = $detail->sum('subtotal');
        $c_detail = $detail->count('id');
        return view('transactions.detail', [
            'transaction' => $transaction,
            'detail' => $detail,
            'c_detail' => $c_detail,
            'totals' => $totals
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Detail::find($id);
        $product = Product::all();
        $customer = Customer::all();
        $transaction = Sale::all();
        return view('transactions.edit', [
            'detail' => $detail,
            'transactions' => $transaction,
            'customer' => $customer,
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detail = Detail::find($id);
        $detail->qty = $request->qty;
        $detail->price = $request->price;
        $detail->subtotal = ($request->qty * $request->price);
        $detail->product_id = $request->product;
        $detail->sale_id = $request->transaction;

        $detail->update();
        return redirect()->route('transactions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sale::where('id',$id)->delete();
        Detail::where('id',$id)->delete();
        return redirect()->back();
    }

}
