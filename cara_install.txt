1. Clone the Project
   git clone https://gitlab.com/adhitya_27/mypos.git
2. Buat Database
3. Import db_posku.sql ke Database yg telah dibuat
4. Edit file ".env", setting dan sesuaikan dengan yg kamu punya...(Jika belum ada file ".env" dibuat dulu filenya di folder project, copy isi file ".env.example" paste kan di file ".env", lalu setting.)
5. Ketik "composer update" atau "composer install" di cmd tempat folder projectnya
6. Generate key. Ketik "php artisan key:generate" di cmd tempat folder projectnya
7. Coba dijalankan...
   "php artisan serve"