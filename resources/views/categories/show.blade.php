@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
        <small>Categories Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('categories.index') }}"><i class="fa fa-group"></i>Categories</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Categories Data</h3>
            <div class="pull-right">
                <a href="{{ route('categories.create') }}" class="btn btn-warning btn-flat"> <i class="fa fa-plus"></i> Create</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th class="text-center">Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    @if (Auth::user()->id)
                    <tr>
                        <td class="text-center">{{ $category->id }}</td>
                        <td class="text-center">{{ $category->name }}</td>
                        <td class="text-center">
                        <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> Edit</a> |
                            <form method="POST" id="delete-form-{{ $category->id }}" action="{{ route('categories.destroy', $category->id) }}" style="display: none">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            </form>
                              <a  class="btn btn-danger btn-xs" type="button" onclick="
                              if(confirm('Are you sure, want to delete {{ $category->id }}')) {
                                event.preventDefault(); document.getElementById('delete-form-{{ $category->id }}').submit();
                              } else {
                                event.preventDefault();
                              }
                              ">Delete</a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </section>
@endsection