@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items
        <small>Items Product</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('items.index') }}"><i class="fa fa-group"></i>Items</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Items Product</h3>
            <div class="pull-right">
                <a href="{{ route('items.create') }}" class="btn btn-warning btn-flat"> <i class="fa fa-plus"></i> Create</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th class="text-center">Id</th>
                        <th class="text-center">Category Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Stock</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                    @if(Auth::user()->id)
                    <tr>
                        <td class="text-center">{{$item->id}}</td>
                        <td class="text-center">{{$item->category_id}}</td>
                        <td class="text-center">{{$item->name}}</td>
                        <td class="text-center">{{$item->price}}</td>
                        <td class="text-center">{{$item->stock}}</td>
                        <td class="text-center">
                        <a href="{{ route('items.edit', $item->id)}}" class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> Edit</a>
                            <form method="POST" id="delete-form-{{ $item->id }}" action="{{ route('items.destroy', $item->id) }}" style="display: none">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            </form>
                              <a  class="btn btn-danger btn-xs" type="button" onclick="
                              if(confirm('Are you sure, want to delete {{ $item->id }}')) {
                                event.preventDefault(); document.getElementById('delete-form-{{ $item->id }}').submit();
                              } else {
                                event.preventDefault();
                              }
                              ">Delete</a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </section>
@endsection