@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaction
        <small>Transaction Product</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-shopping-cart"></i>Transaction</a></li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Transaction</h3>
            <div class="pull-right">
                <a href="." class="btn btn-warning btn-flat"> <i class="fa fa-undo"></i> Back</a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <form action="{{route('transactions.store')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="customer">Customer :</label>
                            <input type="hidden" name="id" value="">
                            <select name="customer" id="customer" class="form-control">
                                <option value="">--Pilih--</option>
                                @foreach($customer as $customers)
                                <option value="{{$customers->id}}">{{$customers->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <label>Daftar Barang :</label>
                        <div class="box-body table-responsive">
                        <table class="table  table-striped" id="cart">
                            <thead>
                            <tr>
                                <th class="text-center">Product</th>
                                <th class="text-center">Price</th>
                                <th style="width: 10%;" class="text-center">qty</th>
                                <th class="text-center">Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $total = 0 ?>

                            @if(session('cart'))
                                @foreach(session('cart') as $id => $details)

                                    <?php $total += $details['price'] * $details['qty'] ?>

                                    <tr>
                                        <td class="text-center" data-th="Product">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <h4 class="nomargin">{{ $details['name'] }}</h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center" data-th="Price">Rp. {{ $details['price'] }}</td>
                                        <td class="text-center" data-th="qty">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <h4 class="nomargin">{{ $details['qty'] }}</h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center" data-th="Subtotal" class="text-center">Rp. {{ $details['price'] * $details['qty'] }}</td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr class="visible-xs">
                                <td class="text-center"><strong>Total {{ $total }}</strong></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2" class="hidden-xs"></td>
                                <td class="hidden-xs text-center"><strong>Total Rp. {{ $total }}</strong></td>
                            </tr>
                            </tfoot>
                        </table>
                        </div>

                        <label>Uang pembeli :</label>
                        <div class="input-group margin">
                            <input type="hidden" name="total" value="{{$total}}">
                            <input type="number" name="uang" class="form-control">
                        </div>

                        <div class="form-group">
                            <button type="reset" class="btn pull-right"style="margin-right: 5px;">Reset</button>
                            <button type="submit" name="" class="btn btn-warning pull-right"style="margin-right: 5px;"> <i class="fa fa-paper-plane"></i> Save</button>
                        </div>
                    </form>
            </div>
        </div>
      </div>
    </section>
@endsection