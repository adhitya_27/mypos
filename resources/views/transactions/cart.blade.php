@extends('layouts.app')

@section('content')

    @if(session('success'))

        <div class="alert alert-success margin">
            {{ session('success') }}
        </div>

    @endif
<section class="content">
    <div class="box">
    <h2 class="margin">Daftar barang yang dibeli :</h2>
    <div class="box-body table-responsive">
    <table class="table  table-striped" id="cart">
        <thead>
        <tr>
            <th class="text-center">Product</th>
            <th class="text-center">Price</th>
            <th style="width: 10%;" class="text-center">qty</th>
            <th class="text-center">Subtotal</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php $total = 0 ?>

        @if(session('cart'))
            @foreach(session('cart') as $id => $details)

                <?php $total += $details['price'] * $details['qty'] ?>

                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">Rp. {{ $details['price'] }}</td>
                    <td data-th="qty">
                        <input type="number" value="{{ $details['qty'] }}" class="qty form-control" />
                    </td>
                    <td data-th="Subtotal" class="text-center">Rp. {{ $details['price'] * $details['qty'] }}</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif

        </tbody>
        <tfoot>
        <tr class="visible-xs">
            <td class="text-center"><strong>Total {{ $total }}</strong></td>
        </tr>
        <tr>
            <td><a href="{{ url('/transactions/create') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i>&nbsp;Lanjutkan</a>
            <a href="/pilih/barang" class="btn btn-default"><i class="fa fa-undo"></i>&nbsp;Back</a>
            </td>
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong>Total Rp. {{ $total }}</strong></td>
        </tr>
        </tfoot>
    </table>
    </div>
    </div>
</section>
@endsection

