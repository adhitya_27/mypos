@extends('layouts.app')

@section('content')

@if($c_detail != 0)
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaction
        <small>Detail</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-shopping-cart"></i>Transaction</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Detail Transaction</h3>
            <div class="pull-right">
                <a href="{{ route('transactions.index') }}" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> Back</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th class="text-center">Transaction Id</th>
                        <th class="text-center">Detail Id</th>
                        <th class="text-center">Product Id</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Subtotal</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail as $details)
                    @if(Auth::user()->id)
                    <tr>
                        <td class="text-center">{{ $details->sale_id }}</td>
                        <td class="text-center">{{ $details->id }}</td>
                        <td class="text-center">{{ $details->product_id }}</td>
                        <td class="text-center">{{ $details->qty }}</td>
                        <td class="text-center">{{ $details->price }}</td>
                        <td class="text-center">{{ $details->subtotal }}</td>
                        <td class="text-center">
                        @foreach($transaction as $transactions)
                        @if($transactions->total == '')
                        <a href="{{ route('transactions.edit', $details->id)}}" class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> Edit</a>
                        @else
                        @endif
                        @endforeach
                            <form method="POST" id="delete-form-{{ $details->id }}" action="{{ route('transactions.destroy', $details->id) }}" style="display: none">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            </form>
                              <a  class="btn btn-danger btn-xs" type="button" onclick="
                              if(confirm('Are you sure, want to delete {{ $details->id }}')) {
                                event.preventDefault(); document.getElementById('delete-form-{{ $details->id }}').submit();
                              } else {
                                event.preventDefault();
                              }
                              ">Delete</a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
            <h2>Total: {{$totals}}</h2>
            </div>
        </div>
      </div>

    @if($transactions->total =='')  
    <div class="box">
        <div class="box-header">
            <h2>
            <p class="margin" style="margin-bottom: 20px;">Uang Pembeli</p>
            </h2>
            <form method="POST" action="{{ route('details.store') }}">
            {{ csrf_field() }}
            <div class="input-group margin">
                <input type="hidden" name="id" value="{{$details->sale_id}}">
                <input type="hidden" name="total" value="{{$totals}}">
                <input type="number" name="uang" class="form-control">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-warning btn-flat">Bayar!</button>
                    </span>
            </div>
            </form>
        </div>
    </div>
    @else
    @endif
    </section>
@else

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaction
        <small>Detail</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-group"></i>Transaction</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Detail Transaction</h3>
            <div class="pull-right">
                <a href="{{ route('transactions.index') }}" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> Back</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="alert alert-danger alert-dismissible margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Data Kosong!</h4>
            Empty!
            </div>
            <div class="pull-right">
            </div>
        </div>
      </div>
    

    </section>

@endif
@endsection