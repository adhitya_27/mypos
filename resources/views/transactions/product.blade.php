@extends('layouts.app')
 
@section('content')
 
    <div class="container products">
        
        <div class="row margin" style="background-color: white;">
            <h2 class="margin">Pilih Barang yang mau dibeli :
            </h2>
            <div class="col-sm-12" style="margin-bottom: 15px;">
                <form action="/pilih/barang/" method="get">
                <div class="input-group input-group-sm">
                <input type="text" class="form-control" name="search">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat">Cari</button>
                    </span>

                </div>
                </form>
            </div>
        </div>
        <div class="row">
 
            @foreach($products as $product)
                <div class="col-xs-18 col-sm-6 col-md-3 margin">
                    <div class="thumbnail">
                        <div class="caption">
                            <h4>{{ $product->id }}</h4>
                            <p>{{ $product->name }}</p>
                            <p><strong>Stock: </strong>{{ $product->stock == '' ? 'Habis' : $product->stock }}</p>
                            <p><strong>Price: </strong>Rp. {{ $product->price }}</p>
                            <p class="btn-holder">
                            @if($product->stock == '')
                                <button href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button" disabled>Add to cart</button>
                            @else
                                <a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Add to cart</a>
                            @endif
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
 
        </div><!-- End row -->

        @if($search != '')
        <div class="row margin">
                <a href="/pilih/barang"><button class="btn btn-default">Back</button></a>
        </div>
        @endif
 
        <div class="row margin" style="background-color: white;">
            <h2 class="margin">Lihat Barang yang dibeli : (<a href="/cart">Show</a>)</h2>
        </div>

    </div>
 
@endsection