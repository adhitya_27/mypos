@extends('layouts.app')

@section('content')
@csrf
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaction
        <small>Transaction Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-shopping-cart"></i>Transaction</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Transaction Data</h3>
            <div class="pull-right">
                <a href="/pilih/barang" class="btn btn-warning btn-flat"> <i class="fa fa-plus"></i> Create</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th class="text-center">Id</th>
                        <th class="text-center">Customer Id</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                    @if(Auth::user()->id)
                    <tr>
                        <td class="text-center">{{$transaction->id}}</td>
                        <td class="text-center">{{$transaction->customer_id}}</td>
                        <td class="text-center">
                            @if($transaction->total != '')
                                <li class="fa fa-check"></li>
                            @else
                                <li class="fa fa-close"></li>
                            @endif
                        </td>
                        <td class="text-center">
                            <form method="POST" id="delete-form-{{ $transaction->id }}" action="{{ route('transactions.destroy', $transaction->id) }}" style="display: none">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                            </form>
                              <a  class="btn btn-danger btn-xs" type="button" onclick="
                              if(confirm('Are you sure, want to delete {{ $transaction->id }}')) {
                                event.preventDefault(); document.getElementById('delete-form-{{ $transaction->id }}').submit();
                              } else {
                                event.preventDefault();
                              }
                              ">Delete</a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </section>
@endsection