@extends('layouts.app')

@section('content')

@if($kembalian > 0)
<div class="alert alert-success alert-dismissible margin">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h4><i class="icon fa fa-check"></i> Success!</h4>
Kembaliannya {{$kembalian}}
</div>
</div>
@endif

@if($kembalian < 0)
  <div class="alert alert-danger alert-dismissible margin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Kurang!</h4>
    Uang {{$kembalian}}
  </div>
@endif

@endsection