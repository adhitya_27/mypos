@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>Add User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-user"></i>Users</a></li>
        <li class="active">Add</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add User</h3>
            <div class="pull-right">
                <a href="{{route('users.index')}}" class="btn btn-warning btn-flat"> <i class="fa fa-undo"></i> Back</a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                <?php //echo validation_errors(); ?>
                    <form action="{{ route('users.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name *</label>
                            <input type="text" class="form-control" name="name" value="">
                        </div>
                        <div class="form-group">
                            <label for="email">Email *</label>
                            <input type="email" class="form-control" name="email" value="">
                            
                        </div>
                        <div class="form-group">
                            <label for="password">Password *</label>
                            <input type="password" class="form-control" name="password" value="">
                            
                        </div>
                        <div class="form-group">
                            <label for="passwordconf">Password Confirmation *</label>
                            <input type="password" class="form-control" name="passwordconf" value="">
                        </div>
                        <div class="form-group">
                            <label for="level">Level *</label>
                            <select name="level" id="level" class="form-control">
                                <option value="">-Select-</option>
                                <option value="1">Admin</option>
                                <option value="2">Kasir</option>
                                <option value="3">Owner</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning"> <i class="fa fa-paper-plane"></i> Save</button>
                            <button type="reset" class="btn btn-flat">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </section>
@endsection