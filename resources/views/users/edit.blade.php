@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>Edit User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-user"></i>Users</a></li>
        <li class="active">Edit</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit User</h3>
            <div class="pull-right">
                <a href="/users/show" class="btn btn-warning btn-flat"> <i class="fa fa-undo"></i> Back</a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="{{ route('users.update', $user->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="name">Name *</label>
                            <input type="hidden" name="id" value="">
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                            
                        </div>
                        <div class="form-group">
                            <label for="email">Email *</label>
                            <input type="email" class="form-control" name="email" value="{{$user->email}}">
                            
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" class="form-control" name="password" value="{{$user->password}}">
                            
                        </div>
                        <div class="form-group">
                            <label for="passwordconf">Password Confirmation</label>
                            <input type="password" class="form-control" name="passwordconf" value="{{$user->passwordconf}}">
                            
                        </div>
                        <div class="form-group">
                            <label for="level">Level</label>
                            <select name="level" id="level" class="form-control">
                                
                                <option value="1" {{$user->level==1 ? 'selected' : ''}}>Admin</option>
                                <option value="2" {{$user->level==2 ? 'selected' : ''}}>Kasir</option>
                            </select>
                            
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning"> <i class="fa fa-paper-plane"></i> Save</button>
                            <button type="reset" class="btn btn-flat">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </section>
@endsection