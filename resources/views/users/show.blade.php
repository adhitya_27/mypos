@extends('layouts.app')

@section('title', 'MyPos')

@section('content')  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>List Users</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-group"></i>Users</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Users</h3>
            <div class="pull-right">
                <a href="{{ route('users.create') }}" class="btn btn-warning btn-flat"> <i class="fa fa-plus"></i>Create</a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th class="text-center">Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Password</th>
                        <th class="text-center">Level</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    @if ( Auth::user()->id)
                    <tr>
                        <td class="text-center">{{$user->id}}</td>
                        <td class="text-center">{{$user->name}}</td>
                        <td class="text-center">{{$user->email}}</td>
                        <td class="text-center">{{$user->password}}</td>
                        <td class="text-center">{{$user->level}}</td>
                        <td class="text-center">
                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> Edit</a>
                                <form method="POST" id="delete-form-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" style="display: none">
                                {{ csrf_field() }} {{ method_field('DELETE') }}
                                </form>
                                <a  class="btn btn-danger btn-xs" type="button" onclick="
                                if(confirm('Are you sure, want to delete {{ $user->id }}')) {
                                event.preventDefault(); document.getElementById('delete-form-{{ $user->id }}').submit();
                                } else {
                                  event.preventDefault();
                                }
                                ">Delete</a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </section>
@endsection