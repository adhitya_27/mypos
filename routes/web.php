<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::resource('customers','CustomerController');
Route::resource('categories','CategoryController');
Route::resource('items','ItemController');
Route::resource('details','DetailController');
Route::resource('transactions','TransactionController');
Route::resource('users','UserController');
Route::get('/session/tampil','SesiController@tampilkanSession');
Route::post('/session/buat','SesiController@buatSession');
Route::get('/session/hapus','SesiController@hapusSession');
Route::get('pilih/barang', 'SesiController@index');
Route::get('cart', 'SesiController@cart');
Route::get('add-to-cart/{id}', 'SesiController@addToCart');
Route::patch('update-cart', 'SesiController@update');
 
Route::delete('remove-from-cart', 'SesiController@remove');

Route::get('/home', 'HomeController@index')->name('home');
